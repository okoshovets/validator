local M = {}

local missing = function (key, expected)
	return string.format("Missing %s key (expected %s)", key, expected)
end

local invalid = function (key, got, expected)
	return string.format("Invalid %s key: got %s, expected %s", key, got, expected)
end

function M.any (v)
	return true, v
end

function M.exists (v, key)
	if v == nil then
		return false, string.format("value %s does not exits", key)
	end

	return true, v
end

local type_checker = function (t, key)
	return function (v)
		if v == nil then
			return false, missing(key, t)
		end

		if type(v) ~= t then
			return false, invalid(key, type(v), t)
		end

		return true, v
	end
end

local basic_actor = function (actor, key)
	return function(v)
		local new_v, err = actor(v)
		if new_v ~= nil then
			return true, new_v
		end

		return false, err or invalid(key, tostring(v), 'not nil after actor function')
	end
end

local function check(checkers)
	return function (t)
		if type(t) ~= 'table' then
			error(string.format("Got non-table value to validate: %s", tostring(t)), 2)
		end

		for k, checker in pairs(checkers) do
			local ok, value, rename = checker(t[k], k)
			if not ok then
				error(value, 2)
			end
			if value ~= nil then
				if rename then
					if t[rename] then
						error(string.format("renaming conflict %s->%s", k, rename), 2)
					end
					t[rename] = value
					t[k] = nil
				else
					t[k] = value
				end
			end
		end

		return true
	end
end

local value_validator = function (rule)
	local actor   = rule.actor   or rule.a
	local checker = rule.checker or rule.c
	local rename  = rule.rename  or rule.r
	return function(v)
		if checker then
			local ok, err = checker(v)
			if not ok then
				return false, err or string.format("Error occured on value check: %s", tostring(v))
			end
		end

		if actor then
			v = actor(v)
		end

		return true, v, rename
	end
end
M.value_validator = value_validator

local function validator(schema)
	local checkers = {}
	for k, rule in pairs(schema) do
		if type(rule) == 'string' then
			if rule:match('^*') then
				local checker = _G[rule:sub(2,-1)]
				if type(checker) ~= 'function' then
					error(string.format("No valid checker-function with name=%s", rule:sub(2,-1)))
				end
				checkers[k] = basic_actor(checker, k)
			else
				checkers[k] = type_checker(rule, k)
			end
		elseif type(rule) == 'table' then
			checkers[k] = validator(rule)
		elseif type(rule) == 'function' then
			checkers[k] = rule
		else
			error("Not a valid rule in key %s with value %s", k, tostring(rule))
		end
	end
	return check(checkers)
	--return checkers
end
M.validator = validator

return M

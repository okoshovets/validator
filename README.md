# Validator

Validator for tables in Lua.
- JITs (I think)
- Has custom handlers
- Can transform data during validation

# Usage

```
local v      = require 'validator'.validator
local vv     = require 'validator'.value_validator
local any    = require 'validator'.any
local exists = require 'validator'.exists

local val1 = v({
	s  = 'string';
	ts = '*tostring';
	tn = '*tonumber';
	ta = {
		n = 'number';
	};
	any    = any;
	exists = exists;
})
val1({ s = 'str', ts = 1, tn = '123', ta = { n = 123 }, exists = {{{{}}}}}) -- ok
val1({ s = 'str', ts = 1, tn = '123', ta = { n = 123 }, any = {{{{}}}}}) -- not ok (missing exists)

local val2 = v({
	user_id = vv{ checker = function (v) return if not box.space.users:get(v) then return false,'USER_NOT_FOUND' end return true end };
})
val2({ user_id = 'some_user_id' }) -- ok, if user exists in db

local val3 = v({
	user_id = vv{
		checker = function (v) return if not box.space.users:get(v) then return false,'USER_NOT_FOUND' end return true end;
		actor   = function (v) return T.users.hash(box.space.users:get(v)) end;
		rename  = 'user'
	};
})
t = { user_id = 'some_user_id }
val3(t) -- ok if user exists, t is transformed to { user = { users_hash_from_db } }

local val4 = v({
	user_id = vv{
		c = function (v) return if not box.space.users:get(v) then return false,'USER_NOT_FOUND' end return true end;
		a = function (v) return T.users.hash(box.space.users:get(v)) end;
		r = 'user'
	};
})
-- val4 is equivalent to val3
```
